import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.21"
    application
}

group = "revolut"
version = "1.0-SNAPSHOT"


repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    val vavrVersion = "0.10.0"
    val junitVersion = "5.2.0"
    val jacsonVersion = "2.9.7"
    implementation(kotlin("stdlib-jdk8"))
    implementation (kotlin("reflect") as String){
        isForce = true
        because("Force update library version for jackson kotlin module")
    }
    implementation("com.sparkjava:spark-core:2.8.0")
    implementation("org.koin:koin-core:1.0.2")
    implementation("io.vavr:vavr-kotlin:$vavrVersion")
    implementation("io.vavr:vavr-jackson:$vavrVersion")
    implementation("org.slf4j:slf4j-simple:1.7.21")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:$jacsonVersion")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:$jacsonVersion")
    
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.0.1")
    testImplementation (kotlin("test-junit5"))
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.0.1")
    testImplementation("com.squareup.retrofit2:retrofit:2.5.0")
    testImplementation("com.squareup.retrofit2:converter-jackson:2.5.0")
    testImplementation("io.mockk:mockk:1.9")
    testImplementation("io.kotlintest:kotlintest-assertions:3.2.1")
    testImplementation("org.junit.jupiter:junit-jupiter-api:$junitVersion")
    testImplementation("org.junit.jupiter:junit-jupiter-params:$junitVersion")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:$junitVersion")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

tasks.withType<Test> {
    useJUnitPlatform()
}

configure<ApplicationPluginConvention> {
    mainClassName = "com.revolut.ApplicationKt"
}
configure<SourceSetContainer>{
    named("test") {
        java.srcDir("src/test-integration/kotlin")
    }
}

