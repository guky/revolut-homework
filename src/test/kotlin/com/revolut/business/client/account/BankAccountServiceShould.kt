package com.revolut.business.client.account

import com.revolut.business.client.Client
import com.revolut.business.client.ClientBankAccountNumber
import com.revolut.business.client.ClientId
import com.revolut.business.client.account.BankAccountService.TransactionBetweenSameClientProhibited
import com.revolut.infra.model.Repository
import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import io.mockk.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.math.BigDecimal

internal class BankAccountServiceShould {
    val clients = mockk<Repository<ClientId, Client>>()
    val clientId = ClientId()
    val client = mockk<Client>(relaxed = true)
    val bankAccountNumber = BankAccountNumber()
    val defaultAmount = BigDecimal(10)
    val transactionProducer = mockk<TransactionProducer>()

    val bankAccountService = BankAccountService(clients)


    @BeforeEach
    fun setUp() {
        every { client.add(allAny()) } returns client
        every { client.balance(clientBankAccount()) } returns defaultAmount
        every { client.fund(clientBankAccount(), any()) } returns client
        every { clients.get(clientId) } returns client
        every { clients.save(client) } returns client

    }

    @Nested
    inner class Transfer {
        val sender = mockk<Client>(relaxed = true)
        val senderClientId = ClientId()
        val receiver = client
        val receiverClientId = clientId

        @BeforeEach
        fun setUp() {
            every { sender getProperty ("id") } returns senderClientId
            every { receiver getProperty ("id") } returns receiverClientId
            every { sender.withdraw(senderClientBankAccountNumber(), any()) } returns sender
            every { clients.get(senderClientId) } returns sender
            every { clients.save(sender) } returns sender
            every { clients.revertTo(any()) } answers { firstArg() }
            excludeRecords { sender getProperty ("id") }
            excludeRecords { receiver getProperty ("id") }
        }

        @Test
        fun `money between accounts`() {
            bankAccountService.transfer(
                senderClientBankAccountNumber(),
                receiverClientBankAccountNumber(),
                defaultAmount,
                transactionProducer
            )

            verifyOrder {
                clients.get(senderClientId)
                clients.get(receiverClientId)
                sender.withdraw(senderClientBankAccountNumber(), any())
                clients.get(senderClientId)
                clients.save(sender)
                receiver.fund(receiverClientBankAccountNumber(), any())
                clients.get(receiverClientId)
                clients.save(receiver)
            }
        }

        @Test
        fun `revert withdrawal on sender if funding fails`() {
            every {
                receiver.fund(
                    receiverClientBankAccountNumber(),
                    any()
                )
            } throws RuntimeException("dummy exception")

            shouldThrow<java.lang.RuntimeException> {
                bankAccountService.transfer(
                    senderClientBankAccountNumber(),
                    receiverClientBankAccountNumber(),
                    defaultAmount,
                    transactionProducer
                )
            }

            verifyOrder {
                clients.get(senderClientId)
                clients.get(receiverClientId)
                sender.withdraw(senderClientBankAccountNumber(), any())
                clients.get(senderClientId)
                clients.save(sender)
                receiver.fund(receiverClientBankAccountNumber(), any())
                clients.revertTo(sender)
            }
        }

        @Test
        fun `not revert withdrawal on sender if it fails`() {
            every {
                sender.withdraw(
                    senderClientBankAccountNumber(),
                    any()
                )
            } throws RuntimeException("dummy exception")

            shouldThrow<java.lang.RuntimeException> {
                bankAccountService.transfer(
                    senderClientBankAccountNumber(),
                    receiverClientBankAccountNumber(),
                    defaultAmount,
                    transactionProducer
                )
            }

            verifyOrder {
                clients.get(senderClientId)
                clients.get(receiverClientId)
                sender.withdraw(senderClientBankAccountNumber(), any())
            }
        }

        @Test
        fun `not transfer money with in one client`() {
            shouldThrow<TransactionBetweenSameClientProhibited> {
                bankAccountService.transfer(
                    senderClientBankAccountNumber(),
                    senderClientBankAccountNumber(),
                    defaultAmount,
                    transactionProducer
                )
            }

            verify(exactly = 0) {
                sender.withdraw(senderClientBankAccountNumber(), any())
                sender.fund(senderClientBankAccountNumber(), any())
            }
        }

        private fun senderClientBankAccountNumber() = ClientBankAccountNumber(senderClientId, bankAccountNumber)
        private fun receiverClientBankAccountNumber() = clientBankAccount()
    }


    @Test
    fun `add bank account to client`() {
        bankAccountService.createBankAccount(clientId)

        verifyOrder {
            clients.get(clientId)
            client.add(any())
            clients.save(client)
        }
    }

    @Test
    fun `correctly return balance`() {
        bankAccountService.balance(clientBankAccount()) shouldBe defaultAmount

        verifyOrder {
            clients.get(clientId)
            client.balance(clientBankAccount())
        }
    }

    @Test
    fun `add funds to client bank account`() {
        bankAccountService.fund(clientBankAccount(), defaultAmount, transactionProducer)

        verifyOrder {
            clients.get(clientId)
            client.fund(clientBankAccount(), any())
            clients.save(client)
        }
    }

    private fun clientBankAccount() = ClientBankAccountNumber(clientId, bankAccountNumber)
}