package com.revolut.business.client.account

import com.revolut.infra.datetime.now
import com.revolut.infra.math.moneyScale
import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import io.mockk.mockk
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.math.BigDecimal



internal class BankAccountShould {
    val defaultTransactionAmount = BigDecimal(10)
    val defaultBankAccountNumber = mockk<BankAccountNumber>()
    val defaultTransactionProducer = mockk<TransactionProducer>()

    val positiveTransaction = Transaction(defaultTransactionAmount, now(), defaultTransactionProducer)
    val negativeTransaction = positiveTransaction.negate()

    val emptyBankAccount = BankAccount(defaultBankAccountNumber)
    val fundedBankAccount = emptyBankAccount.fund(positiveTransaction)

    @Nested
    internal inner class Withdraw {

        @Test
        fun `successfully by negative transaction`() {
            val withdrawnBankAccount = fundedBankAccount.withdraw(negativeTransaction)
            withdrawnBankAccount.balance() shouldBe positiveTransaction.amount + negativeTransaction.amount
        }

        @Test
        fun `unsuccessfully by negative transaction with low balance`() {
            shouldThrow<BankAccount.NotEnoughFunds> {
                emptyBankAccount.withdraw(negativeTransaction)
            }
        }
    }

    @Nested
    internal inner class Fund {

        @Test
        fun `successfully by positive transaction`() {
            val fundedBankAccount = emptyBankAccount.fund(positiveTransaction)
            fundedBankAccount.balance() shouldBe positiveTransaction.amount
        }

        @Test
        fun `unsuccessfully by negative transaction`() {
            shouldThrow<BankAccount.FundShouldBePositive> {
                emptyBankAccount.fund(negativeTransaction)
            }
        }
    }

    @Test
    fun `be empty with out transactions`() {
        emptyBankAccount.isEmpty() shouldBe true
    }

    @Test
    fun `be not empty with transactions`() {
        val bankAccountWithTransactions = emptyBankAccount.fund(positiveTransaction)
        bankAccountWithTransactions.isEmpty() shouldBe false
    }

    @Test
    fun `correctly calculate balance`() {
        val positiveTransactionCount = 3
        val positiveNegativeCount = 2
        val fundedBankAccount = (0..positiveTransactionCount)
            .fold(emptyBankAccount) { bankAccount, _ -> bankAccount.fund(positiveTransaction) }
        val withrawnBankAccount = (0..positiveNegativeCount)
            .fold(fundedBankAccount) { bankAccount, _ -> bankAccount.withdraw(negativeTransaction) }

        val expectedBalance = defaultTransactionAmount * (positiveTransactionCount - positiveNegativeCount).toBigDecimal()
        withrawnBankAccount.balance() shouldBe expectedBalance.moneyScale()
    }
}