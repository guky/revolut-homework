package com.revolut.business.client.account

import com.revolut.infra.datetime.now
import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import io.mockk.mockk
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import java.math.BigDecimal
import java.math.BigDecimal.ZERO

internal class TransactionShould {
    val defaultProducer = mockk<TransactionProducer>()

    @Test
    fun `not be possible to create zero transaction`() {
        shouldThrow<Transaction.ZeroTransactionProhibited> {
            Transaction(ZERO, now(), defaultProducer)
        }
    }

    @Test
    fun `not be possible to create transaction smaller then default scale`() {
        shouldThrow<Transaction.ZeroTransactionProhibited> {
            Transaction(BigDecimal(0.000000001), now(), defaultProducer)
        }
    }

    @ParameterizedTest(name = "{0} expected {1}")
    @CsvSource(
        "1.02,  true",
        "-1.01, false"
    )
    fun `return isPositive as following`(amount: BigDecimal, expected: Boolean) {
        Transaction(amount, now(), defaultProducer).isPositive() shouldBe expected
    }

    @ParameterizedTest(name = "{0} expected {1}")
    @CsvSource(
        "1.02,  false",
        "-1.01, true"
    )
    fun `return isNegative as following`(amount: BigDecimal, expected: Boolean) {
        Transaction(amount, now(), defaultProducer).isNegative() shouldBe expected
    }
}