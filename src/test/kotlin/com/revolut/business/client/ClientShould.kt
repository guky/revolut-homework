package com.revolut.business.client

import com.revolut.business.client.account.BankAccount
import com.revolut.business.client.account.BankAccountNumber
import com.revolut.business.client.account.Transaction
import com.revolut.business.client.account.TransactionProducer
import com.revolut.infra.datetime.now
import io.kotlintest.matchers.collections.shouldContainExactlyInAnyOrder
import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import java.math.BigDecimal

internal class ClientShould {

    val defaultProducer = mockk<TransactionProducer>()
    val transactionAmount = BigDecimal(10)
    val bankAccountNumber = BankAccountNumber()
    val emptyBankAccount = setupBankAccount(empty = true, bankAccountNumber = bankAccountNumber)

    val client = Client("firstName", "lastName").add(emptyBankAccount)

    @Test
    fun `successfully withdraw from bank account`() {
        val transaction = Transaction(transactionAmount, now(), defaultProducer).negate()
        client.withdraw(clientBankAccount(), transaction)

        verify { emptyBankAccount.withdraw(transaction) }
    }

    @Test
    fun `successfully fund to bank account`() {
        val transaction = Transaction(transactionAmount, now(), defaultProducer)
        client.fund(clientBankAccount(), transaction)

        verify { emptyBankAccount.fund(transaction) }
    }

    @Test
    fun `not add bank account with transactions`() {
        val bankAccountWithTransactions = setupBankAccount(bankAccountNumber = bankAccountNumber)
        shouldThrow<Client.BankAccountIsNotEmpty> {
            client.add(bankAccountWithTransactions)
        }
    }

    @Test
    fun `not add same bank account`() {
        shouldThrow<Client.BankAccountIsNotUnique> {
            client.add(emptyBankAccount)
        }
    }

    @Test
    fun `add bank account`() {
        val additionalBankAccount = setupBankAccount(empty = true, bankAccountNumber = BankAccountNumber())
        val clientWithAdditionalBankAccount = client.add(additionalBankAccount)

        val bankAccounts = clientWithAdditionalBankAccount.bankAccounts().toJavaSet()
        bankAccounts.size shouldBe 2
        bankAccounts shouldContainExactlyInAnyOrder setOf(emptyBankAccount, additionalBankAccount)
    }

    @Test
    fun `correctly return set of bank accounts`() {
        val bankAccounts = client.bankAccounts().toJavaSet()

        bankAccounts shouldContainExactlyInAnyOrder setOf(emptyBankAccount)
    }

    @Test
    fun `correctly return bank account balance`() {
        client.balance(clientBankAccount()) shouldBe transactionAmount

        verify { emptyBankAccount.balance() }
    }

    @Test
    fun `produce new client with incremented version`() {
        client.incrementVersion().version shouldBe client.version + 1
    }

    private fun clientBankAccount() = ClientBankAccountNumber(client.id, bankAccountNumber)

    private fun setupBankAccount(empty: Boolean = false, bankAccountNumber: BankAccountNumber): BankAccount {
        val bankAccount = mockk<BankAccount>()
        every { bankAccount.isEmpty() } returns empty
        every { bankAccount getProperty "number" } returns bankAccountNumber
        every { bankAccount.fund(allAny()) } returns bankAccount
        every { bankAccount.withdraw(allAny()) } returns bankAccount
        every { bankAccount.balance() } returns transactionAmount
        return bankAccount
    }
}