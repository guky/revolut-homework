package com.revolut.business.client

import com.revolut.infra.model.OptimisticLocked
import io.kotlintest.matchers.collections.shouldContainExactlyInAnyOrder
import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.util.concurrent.CyclicBarrier

internal class ClientRepositoryShould {
    val repository = ClientRepository()

    @Test
    fun `save new client successfully`(){
        val client = Client("firstName", "lastName")
        repository.save(client)

        val clientFromRepository = repository.get(client.id)
        clientFromRepository.id shouldBe client.id
        clientFromRepository.version shouldBe client.version + 1
    }


    @Test
    fun `not be possible to save old version of client`(){
        val client = Client("firstName", "lastName")
        repository.save(client)

        shouldThrow<OptimisticLocked> {
            repository.save(client)
        }
    }

    @Test
    fun `be possible to revert to old version of client`(){
        val client = Client("firstName", "lastName")
        repository.save(client)

        val revertedClient = repository.revertTo(client)
        revertedClient shouldBe client
    }

    @Test
    fun `not be possible to revert more then 1 version of client`(){
        val client = Client("firstName", "lastName")
        var updatedClient = repository.save(client)
        val updatedClient2 = repository.save(updatedClient)

        shouldThrow<OptimisticLocked> {
            repository.revertTo(client)
        }
    }

    @Test
    fun `return all clients`(){
        val clients = listOf<Client>() +
                repository.save(Client("firstName", "lastName")) +
                repository.save(Client("firstName", "lastName")) +
                repository.save(Client("firstName", "lastName"))

        repository.list() shouldContainExactlyInAnyOrder clients
    }

    @Nested
    internal inner class Concurrently {
        val client1 = Client("firstName", "lastName")
        val client2 = Client("firstName", "lastName")

        @Test
        fun `successfully save diffident clients`() {
            val barrier = CyclicBarrier(2)
            runBlocking {
                listOf(
                    saveAsync(client1, barrier),
                    saveAsync(client2, barrier)
                ).forEach { it.await() }
            }
        }

        @Test
        fun `not save same client`() {
            val barrier = CyclicBarrier(2)
            shouldThrow<OptimisticLocked> {
                runBlocking {
                    listOf(
                        saveAsync(client1, barrier),
                        saveAsync(client1, barrier)
                    ).forEach { it.await() }
                }
            }
        }

        private fun saveAsync(client: Client, barrier: CyclicBarrier): Deferred<Client> {
            return GlobalScope.async {
                barrier.await()
                repository.save(client)
            }
        }
    }
}