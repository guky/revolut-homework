package com.revolut.integration.config

import com.revolut.integration.api.BankAccountApi
import com.revolut.integration.api.ClientApi
import com.revolut.integration.web.randomPort
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory


val testModule = module {
    single {
        Retrofit.Builder()
            .baseUrl("http://localhost:$randomPort/")
            .addConverterFactory(JacksonConverterFactory.create(get()))
            .build()
    }
    single {
        get<Retrofit>().create(ClientApi::class.java)
    }
    single {
        get<Retrofit>().create(BankAccountApi::class.java)
    }
}