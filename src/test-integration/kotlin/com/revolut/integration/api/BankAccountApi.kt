package com.revolut.integration.api

import com.revolut.business.client.ClientBankAccountNumber
import com.revolut.business.client.ClientId
import com.revolut.business.client.account.api.AccountBalance
import com.revolut.business.client.account.api.AddFunds
import com.revolut.business.client.account.api.ClientBankAccount
import com.revolut.business.client.account.api.TransferMoney
import io.vavr.collection.Set
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path


interface BankAccountApi {
    companion object {
        const val basePath = "/bank-account"
    }

    @POST("$basePath")
    fun createBankAccount(@Body clientId: ClientId): Call<ClientBankAccountNumber>

    @POST("$basePath/transfer")
    fun transferMoney(@Body request: TransferMoney): Call<Unit>

    @POST("$basePath/add-funds")
    fun addFunds(@Body request: AddFunds): Call<Unit>

    @GET("$basePath/{clientId}/list")
    fun listBankAccounts(@Path("clientId") clientId: String): Call<Set<ClientBankAccount>>

    @GET("$basePath/{clientId}/balance/{bankAccountNumber}")
    fun balance(@Path("clientId") clientId: String, @Path("bankAccountNumber") bankAccountNumber: String): Call<AccountBalance>
}