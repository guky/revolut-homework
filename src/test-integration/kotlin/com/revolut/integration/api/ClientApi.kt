package com.revolut.integration.api

import com.revolut.business.client.ClientId
import com.revolut.business.client.api.CreateClient
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST


interface ClientApi {
    @POST("/client")
    fun createClient(@Body createClient: CreateClient): Call<ClientId>
}