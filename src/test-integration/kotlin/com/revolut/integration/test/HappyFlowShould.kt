package com.revolut.integration.test

import com.revolut.business.client.ClientBankAccountNumber
import com.revolut.business.client.ClientId
import com.revolut.business.client.account.BankAccountNumber
import com.revolut.business.client.account.TransactionProducer
import com.revolut.business.client.account.api.AddFunds
import com.revolut.business.client.account.api.ClientBankAccount
import com.revolut.business.client.account.api.TransferMoney
import com.revolut.business.client.api.CreateClient
import com.revolut.infra.datetime.now
import com.revolut.infra.math.moneyScale
import com.revolut.integration.api.BankAccountApi
import com.revolut.integration.api.ClientApi
import io.kotlintest.shouldBe
import io.vavr.collection.Set
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.math.BigDecimal.ZERO

internal class HappyFlowShould : IntegrationTest() {
    val clientApi: ClientApi = get()
    val bankAccountApi: BankAccountApi = get()
    val senderId = createClient()
    val receiverId = createClient()
    val senderBankAccount = createBankAccount(senderId)
    val receiverBankAccount = createBankAccount(receiverId)
    val defaultAmount = BigDecimal(100)

    @BeforeEach
    internal fun setUp() {
        bankAccounts(senderId).size() shouldBe 1
        bankAccounts(receiverId).size() shouldBe 1
    }

    @Test
    fun `pass successfully`() {
        fundSenderForDefaultAmount()
        balance(senderId, senderBankAccount) shouldBe defaultAmount.moneyScale()

        transferFromSenderToReceiver()
        balance(senderId, senderBankAccount) shouldBe ZERO.moneyScale()
        balance(receiverId, receiverBankAccount) shouldBe defaultAmount.moneyScale()
    }

    private fun bankAccounts(clientId: ClientId): Set<ClientBankAccount> = bankAccountApi.listBankAccounts(
        clientId.value.toString()
    ).execute().body()!!


    private fun transferFromSenderToReceiver() {
        val transferMoney = TransferMoney(
            ClientBankAccountNumber(senderId, senderBankAccount),
            ClientBankAccountNumber(receiverId, receiverBankAccount),
            defaultAmount,
            TransactionProducer(senderId, "Test", now())
        )
        val response = bankAccountApi.transferMoney(transferMoney).execute()
        response.code() shouldBe 202
        response.body()
    }

    private fun balance(clientId: ClientId, bankAccountNumber: BankAccountNumber) =
        bankAccountApi.balance(
            clientId.value.toString(),
            bankAccountNumber.value.toString()
        ).execute().body()!!.balance


    private fun fundSenderForDefaultAmount() {
        val addFunds = AddFunds(
            ClientBankAccountNumber(senderId, senderBankAccount),
            defaultAmount,
            TransactionProducer(senderId, "Test", now())
        )
        val response = bankAccountApi.addFunds(addFunds).execute()
        response.code() shouldBe 202
    }

    private fun createBankAccount(clientId: ClientId): BankAccountNumber {
        val response = bankAccountApi.createBankAccount(clientId).execute()
        response.code() shouldBe 201
        return response.body()!!.bankAccountNumber
    }

    private fun createClient() =
        clientApi.createClient(CreateClient("firstName", "lastName")).execute().body()!!

}