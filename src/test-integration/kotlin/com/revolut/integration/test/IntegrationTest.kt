package com.revolut.integration.test

import com.revolut.integration.config.testModule
import com.revolut.integration.web.randomPort
import com.revolut.koinModules
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.koin.core.KoinContext
import org.koin.standalone.StandAloneContext.startKoin
import spark.Spark
import spark.Spark.awaitInitialization
import spark.Spark.stop

internal abstract class IntegrationTest {
    companion object {
        lateinit var context: KoinContext
        @BeforeAll
        @JvmStatic
        fun beforeClass() {
            Spark.port(randomPort)
            context = startKoin(mutableListOf(testModule) + koinModules).koinContext
            awaitInitialization()
        }

        @AfterAll
        @JvmStatic
        fun afterClass() {
            stop()
        }
    }

    protected inline fun <reified T : Any> get(): T = context.get()
}