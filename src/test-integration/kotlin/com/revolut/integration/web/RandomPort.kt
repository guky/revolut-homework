package com.revolut.integration.web

import kotlin.random.Random

const val min = 30_000
const val max = 32_000
val randomPort = Random.nextInt((max - min) + 1) + min

