package com.revolut.infra.math

import java.math.BigDecimal
import java.math.RoundingMode

const val defaultMoneyScale = 8

fun BigDecimal.moneyScale(): BigDecimal {
    return this.setScale(defaultMoneyScale, RoundingMode.HALF_UP)
}