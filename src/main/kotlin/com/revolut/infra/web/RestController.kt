package com.revolut.infra.web

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import spark.Request
import spark.Response

abstract class RestController(val objectMapper: ObjectMapper) {
    protected inline fun <reified T> handleJsonRequest(status: Int, crossinline handler: (T) -> (Any)): (Request, Response) -> Any {
        return { request: Request, response: Response ->
            val value = objectMapper
                .readValue<T>(request.body())
            val result = handler(value)
            response.status(status)
            response.type("application/json")
            result
        }
    }
}