package com.revolut.infra.model

import java.util.*

abstract class UniqueIdentifier(val value: UUID = UUID.randomUUID()) {

    constructor(uuid: String) : this(UUID.fromString(uuid))

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is UniqueIdentifier) return false

        if (value != other.value) return false

        return true
    }

    override fun hashCode(): Int {
        return value.hashCode()
    }

    override fun toString(): String {
        return "UniqueIdentifier(value=$value)"
    }
}