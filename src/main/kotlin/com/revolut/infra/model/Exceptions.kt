package com.revolut.infra.model

class ValueNotFound(message: String) : RuntimeException(message)
class OptimisticLocked(message: String) : RuntimeException(message)