package com.revolut.infra.model

import com.revolut.business.client.Client

interface Repository<ID, VALUE> {
    fun get(id: ID): VALUE
    fun list(): List<VALUE>
    fun save(value: VALUE): Client
    fun revertTo(value: VALUE): Client
}