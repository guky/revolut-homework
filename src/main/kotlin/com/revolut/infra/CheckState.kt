package com.revolut.infra

inline fun check(value: Boolean, errorSupplier: () -> Exception) {
    try {
        check(value)
    } catch (e: IllegalStateException) {
        if (!value) {
            throw errorSupplier()
        }
    }
}