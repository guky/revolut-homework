package com.revolut.infra.datetime

import java.time.ZoneId
import java.time.ZonedDateTime

fun now() = ZonedDateTime.now(ZoneId.of("UTC"))
