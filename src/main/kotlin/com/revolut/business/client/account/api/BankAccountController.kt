package com.revolut.business.client.account.api

import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.databind.JsonMappingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.exc.InvalidFormatException
import com.fasterxml.jackson.databind.exc.MismatchedInputException
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException
import com.revolut.business.client.Client.BankAccountNotFound
import com.revolut.business.client.ClientBankAccountNumber
import com.revolut.business.client.ClientId
import com.revolut.business.client.account.BankAccountNumber
import com.revolut.business.client.account.BankAccountService
import com.revolut.infra.model.ValueNotFound
import com.revolut.infra.web.RestController
import io.vavr.collection.Set
import spark.Request
import spark.Response
import spark.Spark.*


class BankAccountController(
    private val bankAccountService: BankAccountService,
    objectMapper: ObjectMapper
) : RestController(objectMapper) {
    init {
        path("/bank-account") {
            post("", handleJsonRequest(201, this::createBankAccount), objectMapper::writeValueAsString)
            post("/transfer", handleJsonRequest(202, this::transferMoney), objectMapper::writeValueAsString)
            post("/add-funds", handleJsonRequest(202, this::addFunds), objectMapper::writeValueAsString)
            get("/:clientId/list", this.listBankAccounts(), objectMapper::writeValueAsString)
            get(
                "/:clientId/balance/:bankAccountNumber", this.balance(), objectMapper::writeValueAsString
            )
            exception(Exception::class.java) { exception, _, response ->
                val status = when (exception) {
                    is JsonParseException, is MismatchedInputException, is UnrecognizedPropertyException ->
                        400
                    is JsonMappingException, is InvalidFormatException ->
                        422
                    is ValueNotFound, is BankAccountNotFound ->
                        404
                    is RuntimeException ->
                        500
                    else -> throw exception
                }
                response.status(status)
                response.body(exception.message)
            }
        }
    }


    private fun createBankAccount(clientId: ClientId): ClientBankAccountNumber {
        return bankAccountService.createBankAccount(clientId)
    }

    private fun transferMoney(request: TransferMoney) {
        bankAccountService.transfer(request.from, request.to, request.amount, request.producer)
        halt(202)
    }

    private fun addFunds(request: AddFunds) {
        bankAccountService.fund(request.clientBankAccount, request.amount, request.producer)
        halt(202)
    }

    private fun balance(): (Request, Response) -> (AccountBalance) = { request, response ->
        val clientId = ClientId(request.params(":clientId"))
        val bankAccountNumber = BankAccountNumber(request.params(":bankAccountNumber"))
        val clientBankAccount = ClientBankAccountNumber(clientId, bankAccountNumber)
        val balance = bankAccountService.balance(clientBankAccount)
        response.type("application/json")
        AccountBalance(balance)

    }

    private fun listBankAccounts(): (Request, Response) -> Set<ClientBankAccount> = { request, response ->
        val clientId = ClientId(request.params(":clientId"))
        response.type("application/json")
        bankAccountService.list(clientId)
    }
}
