package com.revolut.business.client.account

import com.revolut.business.client.ClientId
import com.revolut.infra.check
import com.revolut.infra.math.moneyScale
import java.math.BigDecimal
import java.time.ZonedDateTime

class Transaction(amount: BigDecimal, val timeStamp: ZonedDateTime, val producedBy: TransactionProducer) {
    val amount: BigDecimal = amount.moneyScale()

    init {
        check(this.amount != BigDecimal.ZERO.moneyScale()) { ZeroTransactionProhibited() }
    }

    fun isPositive() = amount > BigDecimal.ZERO
    fun isNegative() = amount < BigDecimal.ZERO
    fun negate() = Transaction(amount.negate(), timeStamp, producedBy)

    class ZeroTransactionProhibited : RuntimeException("Zero transaction prohibited")
}

class TransactionProducer(val iniciator: ClientId, val source: String, val timeStamp: ZonedDateTime)