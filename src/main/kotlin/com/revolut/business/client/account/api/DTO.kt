package com.revolut.business.client.account.api

import com.revolut.business.client.ClientBankAccountNumber
import com.revolut.business.client.ClientId
import com.revolut.business.client.account.BankAccount
import com.revolut.business.client.account.TransactionProducer
import java.math.BigDecimal

data class AccountBalance(val balance: BigDecimal)

data class AddFunds(
    val clientBankAccount: ClientBankAccountNumber,
    val amount: BigDecimal,
    val producer: TransactionProducer
)

data class TransferMoney(
    val from: ClientBankAccountNumber,
    val to: ClientBankAccountNumber,
    val amount: BigDecimal,
    val producer: TransactionProducer
)

data class ClientBankAccount(
    val clientBankAccount: ClientBankAccountNumber,
    val empty: Boolean,
    val balance: BigDecimal
) {
    companion object {
        fun from(bankAccount: BankAccount, clientId: ClientId): ClientBankAccount =
            ClientBankAccount(
                ClientBankAccountNumber(clientId, bankAccount.number),
                bankAccount.isEmpty(),
                bankAccount.balance()
            )
    }
}