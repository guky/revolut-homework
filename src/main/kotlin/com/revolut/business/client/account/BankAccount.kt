package com.revolut.business.client.account

import com.revolut.infra.check
import com.revolut.infra.model.UniqueIdentifier
import io.vavr.collection.List
import io.vavr.kotlin.list
import java.math.BigDecimal
import java.math.BigDecimal.ZERO

class BankAccount(val number: BankAccountNumber = BankAccountNumber()) {
    private var transactions = list<Transaction>()

    fun withdraw(transaction: Transaction): BankAccount {
        check(transaction.isNegative()) { WithdrawalShouldBeNegative(transaction.amount) }
        val balance = balance()
        check(balance >= transaction.amount.negate()) { NotEnoughFunds(transaction.amount, balance) }
        return add(transaction)
    }

    fun fund(transaction: Transaction): BankAccount {
        check(transaction.isPositive()) { FundShouldBePositive(transaction.amount) }
        return add(transaction)
    }

    private fun add(transaction: Transaction): BankAccount = with(transactions.push(transaction))

    private fun with(transactions: List<Transaction>): BankAccount {
        val bankAccount = BankAccount(number)
        bankAccount.transactions = transactions
        return bankAccount
    }

    fun balance() = transactions.map { it.amount }.fold(ZERO) { sum, value -> sum + value }!!

    fun isEmpty() = transactions.isEmpty



    override fun toString(): String {
        return "BankAccount(number=$number)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is BankAccount) return false

        if (number != other.number) return false

        return true
    }

    override fun hashCode(): Int {
        return number.hashCode()
    }

    class WithdrawalShouldBeNegative(amount: BigDecimal) :
        RuntimeException("Withdrawal transaction should be negative amount, but was [$amount]")

    class FundShouldBePositive(amount: BigDecimal) :
        RuntimeException("Fund transaction should be negative amount, but was [$amount]")

    class NotEnoughFunds(amount: BigDecimal, balance: BigDecimal) :
        RuntimeException("Balance to low to withdraw. Balance:[$balance], Transaction amount [$amount]")


}

class BankAccountNumber : UniqueIdentifier {
    constructor() : super()
    constructor(uuid: String) : super(uuid)
}

