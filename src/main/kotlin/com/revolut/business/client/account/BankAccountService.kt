package com.revolut.business.client.account

import com.revolut.business.client.Client
import com.revolut.business.client.ClientBankAccountNumber
import com.revolut.business.client.ClientId
import com.revolut.business.client.account.api.ClientBankAccount
import com.revolut.infra.check
import com.revolut.infra.datetime.now
import com.revolut.infra.model.Repository
import io.vavr.collection.Set
import java.math.BigDecimal

class BankAccountService(private val clients: Repository<ClientId, Client>) {
    fun transfer(
        from: ClientBankAccountNumber,
        to: ClientBankAccountNumber,
        amount: BigDecimal,
        by: TransactionProducer
    ) {
        check(from.clientId != to.clientId) {
            TransactionBetweenSameClientProhibited(to.clientId)
        }
        val clientFrom = clients.get(from.clientId)
        val clientTo = clients.get(to.clientId)
        val fundTransaction = Transaction(amount, now(), by)
        val withdrawTransaction = fundTransaction.negate()


        InTransaction {
            save(clientFrom.withdraw(from, withdrawTransaction))
            save(clientTo.fund(to, fundTransaction))
        }
    }

    fun createBankAccount(clientId: ClientId): ClientBankAccountNumber {
        val client = clients.get(clientId)
        val bankAccount = BankAccount()
        clients.save(client.add(bankAccount))
        return ClientBankAccountNumber(clientId, bankAccount.number)
    }

    fun balance(clientBankAccount: ClientBankAccountNumber): BigDecimal {
        val client = clients.get(clientBankAccount.clientId)
        return client.balance(clientBankAccount)
    }

    fun fund(clientBankAccount: ClientBankAccountNumber, amount: BigDecimal, by: TransactionProducer) {
        var client = clients.get(clientBankAccount.clientId)
        val fundTransaction = Transaction(amount, now(), by)
        client = client.fund(clientBankAccount, fundTransaction)
        clients.save(client)
    }

    fun list(clientId: ClientId): Set<ClientBankAccount> {
        var client = clients.get(clientId)
        return client.bankAccounts().map {
            ClientBankAccount.from(it, clientId)
        }
    }

    inner class InTransaction(transactionalOperation: InTransaction.() -> Unit) {
        private var clientStates: MutableList<Client> = arrayListOf()

        init {
            try {
                transactionalOperation()
            } catch (e: Exception) {
                rollback()
                throw e
            }
        }

        fun save(client: Client) {
            clientStates.add(clients.get(client.id))
            clients.save(client)
        }

        private fun rollback() {
            clientStates.asReversed().forEach {
                clients.revertTo(it)
            }
        }
    }

    class TransactionBetweenSameClientProhibited(clientId: ClientId) :
        RuntimeException("Money transfer with in one client Prohibited. Client id [$clientId]")
}
