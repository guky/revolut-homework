package com.revolut.business.client

import com.revolut.business.client.account.BankAccount
import com.revolut.business.client.account.BankAccountNumber
import com.revolut.business.client.account.Transaction
import com.revolut.infra.check
import com.revolut.infra.model.UniqueIdentifier
import io.vavr.collection.HashMap
import io.vavr.kotlin.hashMap

class Client {
    val id: ClientId
    private var _version = 0L
    val version
        get() = _version
    val firstName: String
    val lastName: String
    private var bankAccounts = hashMap<BankAccountNumber, BankAccount>()

    constructor(firstName: String, lastName: String) : this(ClientId(), firstName, lastName)

    private constructor(id: ClientId, firstName: String, lastName: String) {
        this.id = id
        this.firstName = firstName
        this.lastName = lastName
    }

    fun withdraw(bankAccountNumber: ClientBankAccountNumber, transaction: Transaction): Client {
        val bankAccount = bankAccount(bankAccountNumber).withdraw(transaction)
        return with(bankAccounts.put(bankAccount.number, bankAccount))
    }

    fun fund(bankAccountNumber: ClientBankAccountNumber, transaction: Transaction): Client {
        val bankAccount = bankAccount(bankAccountNumber).fund(transaction)
        return with(bankAccounts.put(bankAccount.number, bankAccount))
    }

    fun add(bankAccount: BankAccount): Client {
        check(bankAccount.isEmpty()) { BankAccountIsNotEmpty(this, bankAccount) }
        check(!bankAccounts.containsKey(bankAccount.number)) { BankAccountIsNotUnique(this, bankAccount) }
        return with(bankAccounts.put(bankAccount.number, bankAccount))
    }

    fun bankAccounts() = bankAccounts.values().toSet()

    fun balance(bankAccountNumber: ClientBankAccountNumber) = bankAccount(bankAccountNumber).balance()

    private fun bankAccount(clientBankAccountNumber: ClientBankAccountNumber): BankAccount {
        check(id == clientBankAccountNumber.clientId) {
            throw IllegalBankAccountAccess(this, clientBankAccountNumber)
        }
        return bankAccounts[clientBankAccountNumber.bankAccountNumber]
            .getOrElseThrow {
                throw BankAccountNotFound(this, clientBankAccountNumber)
            }
    }

    private fun with(bankAccounts: HashMap<BankAccountNumber, BankAccount>): Client {
        val client = Client(id, firstName, lastName)
        client.bankAccounts = bankAccounts
        client._version = _version
        return client
    }

    fun incrementVersion(): Client {
        val client = Client(id, firstName, lastName)
        client.bankAccounts = bankAccounts
        client._version = _version + 1
        return client
    }

    override fun toString(): String {
        return "Client(id=$id, firstName='$firstName', lastName='$lastName')"
    }

    class IllegalBankAccountAccess(client: Client, bankAccountNumber: ClientBankAccountNumber) :
        RuntimeException("ClientBankAccountNumber[$bankAccountNumber] does not belong to client [$client]")

    class BankAccountNotFound(client: Client, bankAccountNumber: ClientBankAccountNumber) :
        RuntimeException("ClientBankAccountNumber[$bankAccountNumber] not found in client [$client]")

    class BankAccountIsNotEmpty(client: Client, bankAccount: BankAccount) :
        RuntimeException("Only empty bank account could be added [$bankAccount] to client [$client]")

    class BankAccountIsNotUnique(client: Client, bankAccount: BankAccount) :
        RuntimeException("Bank account number must be unique [$bankAccount] for client [$client]")
}

class ClientId : UniqueIdentifier {
    constructor() : super()
    constructor(uuid: String) : super(uuid)
}
data class ClientBankAccountNumber(val clientId: ClientId, val bankAccountNumber: BankAccountNumber)