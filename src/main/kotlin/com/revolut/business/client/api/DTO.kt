package com.revolut.business.client.api

data class CreateClient(val firstName: String, val lastName: String)