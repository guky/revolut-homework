package com.revolut.business.client.api

import com.fasterxml.jackson.databind.ObjectMapper
import com.revolut.business.client.Client
import com.revolut.business.client.ClientId
import com.revolut.infra.model.Repository
import com.revolut.infra.web.RestController
import spark.Spark.path
import spark.Spark.post

class ClientController(
    private val clients: Repository<ClientId, Client>,
    objectMapper: ObjectMapper
) : RestController(objectMapper) {
    init {
        path("/client") {
            post("", handleJsonRequest(201, this::createClient), objectMapper::writeValueAsString)
        }
    }

    private fun createClient(request: CreateClient): ClientId {
        val client = Client(request.firstName, request.lastName)
        return clients.save(client).id
    }
}