package com.revolut.business.client

import com.revolut.infra.check
import com.revolut.infra.model.OptimisticLocked
import com.revolut.infra.model.Repository
import com.revolut.infra.model.ValueNotFound
import io.vavr.kotlin.option
import java.util.concurrent.ConcurrentHashMap

class ClientRepository : Repository<ClientId, Client> {
    private val clients = ConcurrentHashMap<ClientId, Client>()

    override fun list(): List<Client> = clients.values.toList()

    override fun get(id: ClientId): Client {
        return clients[id].option()
            .getOrElseThrow { ValueNotFound("Client with id: [$id] not found") }
    }

    override fun save(value: Client): Client {
        return clients.compute(value.id) { _, currentValue ->
            check(value.version == getVersion(currentValue)) {
                OptimisticLocked("Failed to save client [$value] since it was already modified. Please try again")
            }
            return@compute value.incrementVersion()
        }!!
    }

    override fun revertTo(value: Client): Client {
        return clients.compute(value.id) { _, currentValue ->
            check(value.version == getVersion(currentValue) - 1) {
                OptimisticLocked("Failed to revert client [$value] only single step backward possible")
            }
            return@compute value
        }!!
    }

    private fun getVersion(client: Client?) = client?.version ?: 0
}