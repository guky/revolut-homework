package com.revolut.business.config

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.revolut.business.client.Client
import com.revolut.business.client.ClientId
import com.revolut.business.client.ClientRepository
import com.revolut.business.client.account.BankAccountService
import com.revolut.business.client.account.api.BankAccountController
import com.revolut.business.client.api.ClientController
import com.revolut.infra.model.Repository
import io.vavr.jackson.datatype.VavrModule
import org.koin.dsl.module.module


val bankAccountModule = module {
    single { BankAccountService(get()) }
    single(createOnStart = true) { BankAccountController(get(), get()) }
}

val clientModule = module {
    single<Repository<ClientId, Client>> { ClientRepository() }
    single(createOnStart = true) { ClientController(get(), get()) }
    single {
        jacksonObjectMapper()
            .registerModule(VavrModule())
            .registerModule(JavaTimeModule())
    }
}