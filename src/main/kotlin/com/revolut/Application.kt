package com.revolut

import com.revolut.business.config.bankAccountModule
import com.revolut.business.config.clientModule
import org.koin.standalone.StandAloneContext.startKoin
val koinModules = listOf(bankAccountModule, clientModule)

fun main() {
    startKoin(koinModules)
}